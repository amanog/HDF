package org.neosoft.utility;

import static org.testng.Assert.assertEquals;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.neosoft.reporting.ExtentManager;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class BaseClass
{
	protected ExtentReports extent;
	protected ExtentTest test;
	protected String filePath;
	protected String data[][];
	protected int k;


	protected WebDriver driver;
	String excelFile;
	String sheetName;
	XSSFWorkbook wb;
	XSSFSheet ws;
	XSSFFont font;
	XSSFCellStyle style;
	XSSFRow row;
	XSSFRow row1;
	XSSFCell cell;
	XSSFCell cell1;
	FileInputStream fis;
	FileOutputStream fos;
	File excel;

	@BeforeMethod
	public void beforeMethod()
	{
		row1 = ws.getRow(k);
		cell1 = row1.createCell(11);
		extent = ExtentManager.getReporter(filePath);
	}

	@AfterMethod
	public void afterMethod(ITestResult result) throws Exception
	{
		if (result.getStatus() == ITestResult.FAILURE)
		{	
			String screenshot_path = captureScreenShot(driver, result.getName());
			String image = test.addScreenCapture(screenshot_path);
			test.log(LogStatus.FAIL, image);
			test.log(LogStatus.FAIL, result.getThrowable());
			font.setColor(IndexedColors.RED.getIndex());
			style.setFont(font);
			cell1.setCellValue("Fail");
			cell1.setCellStyle(style);

		}
		else if (result.getStatus() == ITestResult.SKIP)
		{
			test.log(LogStatus.SKIP, "Test skipped " + result.getThrowable());
			font.setColor(IndexedColors.BLUE.getIndex());
			style.setFont(font);
			cell1.setCellValue("Skip");
			cell1.setCellStyle(style);
			cell1.setCellValue("Skip");
		}
		else if (result.getStatus() == ITestResult.SUCCESS)
		{
			test.log(LogStatus.PASS, "Test passed");
			font.setColor(IndexedColors.GREEN.getIndex());
			style.setFont(font);
			cell1.setCellValue("Pass");
			cell1.setCellStyle(style);
		}

		extent.endTest(test);        
		extent.flush();
		driver.close();
	}

	@BeforeSuite
	@Parameters({"ef","sn"})
	public void beforeSuite(String ef, String sn) throws Exception
	{
		Calendar calander = Calendar.getInstance();
		SimpleDateFormat formater = new SimpleDateFormat("dd_MM_yy_hh_mm_ss");

		filePath = ".\\reports\\" + formater.format(calander.getTime()) + ".html";
		excelFile = ef;
		sheetName = sn;
		data = ExcelData(excelFile, sheetName);
		extent = ExtentManager.getReporter(filePath);
	}

	@AfterSuite
	public void afterSuite() throws Exception
	{
		extent.close();
		wb.write(fos);
		fos.close();
		wb.close();
		fis.close();
	}

	public static String captureScreenShot(WebDriver driver,String screenshotName)
	{
		try 
		{
			File src= ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			String dest = ".\\screenshots\\"+screenshotName+".png";
			File destination = new File(dest);
			FileUtils.copyFile(src, destination);
			System.out.println("ScreenShot Taken");
			return dest;
		}
		catch (Exception e)
		{
			System.out.println("Exception while taking screenshot"+e.getMessage());
			return e.getMessage();
		}
	}

	public String[][] ExcelData(String path,String sheetname) throws Exception
	{
		excel = new File(path);
		fis = new FileInputStream(excel);
		wb = new XSSFWorkbook(fis);
		ws = wb.getSheet(sheetname);
		style = wb.createCellStyle();
		font = wb.createFont();

		int rowNum = ws.getLastRowNum()+1;
		int colNum = ws.getRow(0).getLastCellNum();

		String[][] data = new String[rowNum][colNum];

		for (int i = 0; i < rowNum; i++)
		{
			row = ws.getRow(i);
			for (int j = 0; j < colNum; j++)
			{
				cell = row.getCell(j);
				String value = cellToString(cell);
				data[i][j] = value;
			}	
		}
		fos = new FileOutputStream(path);

		return data;
	}
	@SuppressWarnings("deprecation")
	private static String cellToString(XSSFCell cell)
	{
		Object result;
		String strReturn = null;

		if (cell == null)
		{
			strReturn = "";
		}
		else
		{
			switch (cell.getCellType())
			{
			case Cell.CELL_TYPE_NUMERIC:
				result = cell.getNumericCellValue();
				strReturn = result.toString();
				break;

			case Cell.CELL_TYPE_STRING:
				result = cell.getStringCellValue();
				strReturn = result.toString();
				break;
			default:
				strReturn = null;
			}
		}
		return strReturn;
	}

	public void browser(String browser, String url)
	{
		if(browser.equalsIgnoreCase("chrome")) 
		{
			extent = ExtentManager.getReporter(filePath);
			System.setProperty("webdriver.chrome.driver",".\\drivers\\chromedriver.exe");
			driver = new ChromeDriver();
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			driver.get(url);
		}

		else if(browser.equalsIgnoreCase("ie")) 
		{
			extent = ExtentManager.getReporter(filePath);
			System.setProperty("webdriver.ie.driver",".\\drivers\\IEDriverServer.exe");
			driver = new InternetExplorerDriver();
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			driver.get(url);
		}

		else if(browser.equalsIgnoreCase("firefox"))
		{
			System.setProperty("webdriver.gecko.driver",".\\drivers\\geckodriver.exe");
			driver = new FirefoxDriver();
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			driver.get(url);
		}
	}

	public void clickElement(String LocType, String LocValue)
	{
		if(LocType.equalsIgnoreCase("id")) 
		{
			driver.findElement(By.id(LocValue)).click();
		}
		else if (LocType.equalsIgnoreCase("xpath"))
		{
			driver.findElement(By.xpath(LocValue)).click();
		}
		else if (LocType.equalsIgnoreCase("linkText"))
		{
			driver.findElement(By.linkText(LocValue)).click();
		}
	}
	public void sendElement(String LocType, String LocValue, String Parameter)
	{
		if(LocType.equalsIgnoreCase("id")) 
		{
			driver.findElement(By.id(LocValue)).sendKeys(Parameter);
		}
		else if (LocType.equalsIgnoreCase("xpath"))
		{
			driver.findElement(By.xpath(LocValue)).sendKeys(Parameter);
		}
		else if (LocType.equalsIgnoreCase("linkText"))
		{
			driver.findElement(By.linkText(LocValue)).sendKeys(Parameter);
		}
	}

	public void selectDropDown(String LocType, String LocValue, String selectBy, String Parameter)
	{	
		if(LocType.equalsIgnoreCase("id"))
		{
			Select dropdown = new Select(driver.findElement(By.id(LocValue)));
			if(selectBy.equalsIgnoreCase("VisibleText"))
			{
				dropdown.selectByVisibleText(Parameter);
			}
			else if(selectBy.equalsIgnoreCase("Index"))
			{
				int intParameter = Integer.parseInt(Parameter);
				dropdown.selectByIndex(intParameter);
			}
		}
		else if (LocType.equalsIgnoreCase("xpath"))
		{
		}
	}

	public void checkElement(String LocType, String LocValue, String Parameter)
	{
		if(LocType.equalsIgnoreCase("id")) 
		{
			assertEquals(driver.findElement(By.id(LocValue)).getText(), Parameter);
		}
		else if(LocType.equalsIgnoreCase("xpath"))
		{
			assertEquals(driver.findElement(By.xpath(LocValue)).getText(), Parameter);
		}
	}

	public void selectElement(String LocValue,String Parameter)
	{
		List<WebElement> list = driver.findElements(By.xpath(LocValue));
		int numberOfEle = list.size();
		int i;
		for(i=0; i<numberOfEle; i++)
		{
			String str = list.get(i).getText();
			System.out.println(str);
			if(str.equalsIgnoreCase(Parameter))
			{
				i++;
				WebElement element = driver.findElement(By.xpath(".//*[@id='resultTable']/tbody/tr["+i+"]/td[1]/input"));
				element.click();
			}
		}
	}
}

