package org.neosoft.test;

import org.neosoft.utility.BaseClass;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

public class ModuleName_02 extends BaseClass
{
	@Test (testName="AdminLogin",groups="Logins",enabled=false)
	public void LoginAdmin()
	{
		for (k = 2; k <=5; k++)
		{
			if(data[k][5].equalsIgnoreCase("browser"))
			{
				browser(data[k][8], data[k][9]);
				test.log(LogStatus.INFO, data[k][4]);
			}
			else if(data[k][5].equalsIgnoreCase("sendElement"))
			{
				sendElement(data[k][6], data[k][7], data[k][8]);
				test.log(LogStatus.INFO, data[k][4]);
			}
			else if(data[k][5].equalsIgnoreCase("clickElement"))
			{
				clickElement(data[k][6], data[k][7]);
				test.log(LogStatus.INFO, data[k][4]);
			}
			else if(data[k][5].equalsIgnoreCase("selectDropDown"))
			{
				selectDropDown(data[k][6], data[k][7], data[k][8], data[k][9]);
				test.log(LogStatus.INFO, data[k][4]);
			}
			else if(data[k][5].equalsIgnoreCase("checkElement"))
			{
				checkElement(data[k][6], data[k][7], data[k][8]);
				test.log(LogStatus.INFO, data[k][4]);
			}
			else if(data[k][5].equalsIgnoreCase("selectElement"))
			{
				selectElement(data[k][7], data[k][8]);
				test.log(LogStatus.INFO, data[k][4]);
			}
		}
	}

	@Test (testName="add_employee",groups="AddEmployee")
	public void AddEmployee()
	{
		test = extent.startTest("TC_003");
		LoginAdmin();
		for (k = 8; k <=15; k++)
		{
			if(data[k][5].equalsIgnoreCase("browser"))
			{
				browser(data[k][8], data[k][9]);
				test.log(LogStatus.INFO, data[k][4]);
			}
			else if(data[k][5].equalsIgnoreCase("sendElement"))
			{
				sendElement(data[k][6], data[k][7], data[k][8]);
				test.log(LogStatus.INFO, data[k][4]);
			}
			else if(data[k][5].equalsIgnoreCase("clickElement"))
			{
				clickElement(data[k][6], data[k][7]);
				test.log(LogStatus.INFO, data[k][4]);
			}
			else if(data[k][5].equalsIgnoreCase("selectDropDown"))
			{
				selectDropDown(data[k][6], data[k][7], data[k][8], data[k][9]);
				test.log(LogStatus.INFO, data[k][4]);
			}
			else if(data[k][5].equalsIgnoreCase("checkElement"))
			{
				checkElement(data[k][6], data[k][7], data[k][8]);
				test.log(LogStatus.INFO, data[k][4]);
			}
			else if(data[k][5].equalsIgnoreCase("selectElement"))
			{
				selectElement(data[k][7], data[k][8]);
				test.log(LogStatus.INFO, data[k][4]);
			}
		}
	}
	
	@Test (testName="add_jobtitle",groups="JobTitle")
	public void AddJobTitle()
	{
		test = extent.startTest("TC_004");
		LoginAdmin();
		for (k = 18; k <=24; k++)
		{
			if(data[k][5].equalsIgnoreCase("browser"))
			{
				browser(data[k][8], data[k][9]);
				test.log(LogStatus.INFO, data[k][4]);
			}
			else if(data[k][5].equalsIgnoreCase("sendElement"))
			{
				sendElement(data[k][6], data[k][7], data[k][8]);
				test.log(LogStatus.INFO, data[k][4]);
			}
			else if(data[k][5].equalsIgnoreCase("clickElement"))
			{
				clickElement(data[k][6], data[k][7]);
				test.log(LogStatus.INFO, data[k][4]);
			}
			else if(data[k][5].equalsIgnoreCase("selectDropDown"))
			{
				selectDropDown(data[k][6], data[k][7], data[k][8], data[k][9]);
				test.log(LogStatus.INFO, data[k][4]);
			}
			else if(data[k][5].equalsIgnoreCase("checkElement"))
			{
				checkElement(data[k][6], data[k][7], data[k][8]);
				test.log(LogStatus.INFO, data[k][4]);
			}
			else if(data[k][5].equalsIgnoreCase("selectElement"))
			{
				selectElement(data[k][7], data[k][8]);
				test.log(LogStatus.INFO, data[k][4]);
			}
		}
	}
	
	@Test (testName="delete_jobtitle",groups="JobTitle")
	public void DeleteJobTitle()
	{
		test = extent.startTest("TC_005");
		LoginAdmin();
		for (k = 27; k <=32; k++)
		{
			if(data[k][5].equalsIgnoreCase("browser"))
			{
				browser(data[k][8], data[k][9]);
				test.log(LogStatus.INFO, data[k][4]);
			}
			else if(data[k][5].equalsIgnoreCase("sendElement"))
			{
				sendElement(data[k][6], data[k][7], data[k][8]);
				test.log(LogStatus.INFO, data[k][4]);
			}
			else if(data[k][5].equalsIgnoreCase("clickElement"))
			{
				clickElement(data[k][6], data[k][7]);
				test.log(LogStatus.INFO, data[k][4]);
			}
			else if(data[k][5].equalsIgnoreCase("selectDropDown"))
			{
				selectDropDown(data[k][6], data[k][7], data[k][8], data[k][9]);
				test.log(LogStatus.INFO, data[k][4]);
			}
			else if(data[k][5].equalsIgnoreCase("checkElement"))
			{
				checkElement(data[k][6], data[k][7], data[k][8]);
				test.log(LogStatus.INFO, data[k][4]);
			}
			else if(data[k][5].equalsIgnoreCase("selectElement"))
			{
				selectElement(data[k][7], data[k][8]);
				test.log(LogStatus.INFO, data[k][4]);
			}
		}
	}
}
