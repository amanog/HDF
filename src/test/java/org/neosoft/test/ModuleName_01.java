package org.neosoft.test;

import org.testng.annotations.Test;

import org.neosoft.utility.BaseClass;
import com.relevantcodes.extentreports.LogStatus;

public class ModuleName_01 extends BaseClass
{
	//@Test (testName="login_admin",groups="Logins")
	public void Login_Admin()
	{
		test = extent.startTest("TC_001");

		for (k = 2; k <=5; k++)
		{
			if(data[k][5].equalsIgnoreCase("browser"))
			{
				browser(data[k][8], data[k][9]);
				test.log(LogStatus.INFO, data[k][4]);
			}
			else if(data[k][5].equalsIgnoreCase("sendElement"))
			{
				sendElement(data[k][6], data[k][7], data[k][8]);
				test.log(LogStatus.INFO, data[k][4]);
			}
			else if(data[k][5].equalsIgnoreCase("clickElement"))
			{
				clickElement(data[k][6], data[k][7]);
				test.log(LogStatus.INFO, data[k][4]);
			}
			else if(data[k][5].equalsIgnoreCase("selectDropDown"))
			{
				selectDropDown(data[k][6], data[k][7], data[k][8], data[k][9]);
				test.log(LogStatus.INFO, data[k][4]);
			}
			else if(data[k][5].equalsIgnoreCase("checkElement"))
			{
				checkElement(data[k][6], data[k][7], data[k][8]);
				test.log(LogStatus.INFO, data[k][4]);
			}
		}
	}

	@Test (testName="login_sysadmin",groups="Logins",enabled=false)
	public void Login_SysAdmin()
	{
		test = extent.startTest("TC_002");

		for (int i = 9; i <=12; i++)
		{
			if(data[i][5].equalsIgnoreCase("browser"))
			{
				browser(data[i][8], data[i][9]);
				test.log(LogStatus.INFO, data[i][4]);
			}
			else if(data[i][5].equalsIgnoreCase("sendElement"))
			{
				sendElement(data[i][6], data[i][7], data[i][8]);
				test.log(LogStatus.INFO, data[i][4]);
			}
			else if(data[i][5].equalsIgnoreCase("clickElement"))
			{
				clickElement(data[i][6], data[i][7]);
				test.log(LogStatus.INFO, data[i][4]);
			}
			else if(data[i][5].equalsIgnoreCase("selectDropDown"))
			{
				selectDropDown(data[i][6], data[i][7], data[i][8], data[i][9]);
				test.log(LogStatus.INFO, data[i][4]);
			}
			else if(data[i][5].equalsIgnoreCase("checkElement"))
			{
				checkElement(data[i][6], data[i][7], data[i][8]);
				test.log(LogStatus.INFO, data[i][4]);
			}
		}
	}

	@Test (testName="add_employee",groups="AddEmployee")
	public void AddEmployee()
	{
		test = extent.startTest("TC_003");
		//Login_Admin();
		for (k = 16; k <=27; k++)
		{
			if(data[k][5].equalsIgnoreCase("browser"))
			{
				browser(data[k][8], data[k][9]);
				test.log(LogStatus.INFO, data[k][4]);
			}
			else if(data[k][5].equalsIgnoreCase("sendElement"))
			{
				sendElement(data[k][6], data[k][7], data[k][8]);
				test.log(LogStatus.INFO, data[k][4]);
			}
			else if(data[k][5].equalsIgnoreCase("clickElement"))
			{
				clickElement(data[k][6], data[k][7]);
				test.log(LogStatus.INFO, data[k][4]);
			}
			else if(data[k][5].equalsIgnoreCase("selectDropDown"))
			{
				selectDropDown(data[k][6], data[k][7], data[k][8], data[k][9]);
				test.log(LogStatus.INFO, data[k][4]);
			}
			else if(data[k][5].equalsIgnoreCase("checkElement"))
			{
				checkElement(data[k][6], data[k][7], data[k][8]);
				test.log(LogStatus.INFO, data[k][4]);
			}
			else if(data[k][5].equalsIgnoreCase("checkElement"))
			{
				selectElement(data[k][7], data[k][8]);
				test.log(LogStatus.INFO, data[k][4]);
			}
		}
	}
}
