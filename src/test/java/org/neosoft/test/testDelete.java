package org.neosoft.test;

import org.neosoft.utility.BaseClass;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

public class testDelete extends BaseClass
{
	@Test(testName="add_employee",groups="AddEmployee")
	public void AddEmployee() throws InterruptedException
	{
		test = extent.startTest("TC_003");
		for (k = 2; k <=9; k++)
		{
			if(data[k][5].equalsIgnoreCase("browser"))
			{
				browser(data[k][8], data[k][9]);
				test.log(LogStatus.INFO, data[k][4]);
			}
			else if(data[k][5].equalsIgnoreCase("sendElement"))
			{
				sendElement(data[k][6], data[k][7], data[k][8]);
				test.log(LogStatus.INFO, data[k][4]);
			}
			else if(data[k][5].equalsIgnoreCase("clickElement"))
			{
				clickElement(data[k][6], data[k][7]);
				test.log(LogStatus.INFO, data[k][4]);
			}
			else if(data[k][5].equalsIgnoreCase("selectDropDown"))
			{
				selectDropDown(data[k][6], data[k][7], data[k][8], data[k][9]);
				test.log(LogStatus.INFO, data[k][4]);
			}
			else if(data[k][5].equalsIgnoreCase("checkElement"))
			{
				checkElement(data[k][6], data[k][7], data[k][8]);
				test.log(LogStatus.INFO, data[k][4]);
			}
			else if(data[k][5].equalsIgnoreCase("selectElement"))
			{
				selectElement(data[k][7], data[k][8]);
				test.log(LogStatus.INFO, data[k][4]);
			}
		}
		Thread.sleep(20000);
	}
}
